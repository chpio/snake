{
	description = "game";

	inputs = {
		nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable-small";
		flake-utils.url = "github:numtide/flake-utils";
	};

	outputs = { self, nixpkgs, flake-utils }:
		flake-utils.lib.eachDefaultSystem (system:
			let pkgs = import nixpkgs { inherit system; };
			in {
				devShell = pkgs.mkShell {
					name = "game";
					buildInputs = with pkgs; [
						cargo
						rustc
						rustfmt
						rust-analyzer

						pkgconfig
						udev
						alsaLib

						vulkan-tools
						vulkan-headers
						vulkan-loader
						vulkan-validation-layers

						x11
						xorg.libXcursor
						xorg.libXrandr
						xorg.libXi
					];
				};
			}
		);
}
