use crate::{cell, game};
use bevy::prelude::*;
use std::{collections::VecDeque, mem};

#[derive(Component)]
pub struct Head;

#[derive(Component)]
pub struct BodySegment;

#[derive(Resource)]
pub struct State {
	pub body_segments: VecDeque<Entity>,
	pub direction: cell::Direction,
	pub next_directions: VecDeque<cell::Direction>,
	pub spawn_new_segment_color: Option<Color>,
}

impl Default for State {
	fn default() -> State {
		State {
			body_segments: VecDeque::new(),
			direction: cell::Direction::Right,
			next_directions: VecDeque::new(),
			spawn_new_segment_color: None,
		}
	}
}

pub fn process_snake_inputs(
	app_state: Res<bevy::ecs::schedule::State<game::State>>,
	keyboard_input: Res<Input<KeyCode>>,
	mut state: ResMut<State>,
) {
	if game::State::InGame != *app_state.current() {
		return;
	}

	let next_direction = if keyboard_input.pressed(KeyCode::Left) {
		Some(cell::Direction::Left)
	} else if keyboard_input.pressed(KeyCode::Right) {
		Some(cell::Direction::Right)
	} else if keyboard_input.pressed(KeyCode::Down) {
		Some(cell::Direction::Down)
	} else if keyboard_input.pressed(KeyCode::Up) {
		Some(cell::Direction::Up)
	} else {
		None
	};

	if let Some(next_direction) = next_direction {
		let back = state
			.next_directions
			.back()
			.copied()
			.unwrap_or_else(|| state.direction);
		if back != next_direction && back != next_direction.reverse() {
			state.next_directions.push_back(next_direction);
		}
	}
}

pub fn move_snake(
	app_state: Res<bevy::ecs::schedule::State<game::State>>,
	mut cmds: Commands,
	mut state: ResMut<State>,
	head: Query<Entity, With<Head>>,
	mut positions: Query<&mut cell::Position>,
) {
	if game::State::InGame != *app_state.current() {
		return;
	}

	let head = head.single();
	let mut head_pos = positions.get_mut(head).unwrap();

	if let Some(next_direction) = state.next_directions.pop_front() {
		state.direction = next_direction;
	}

	let new_head_pos = match state.direction {
		cell::Direction::Left => {
			let x = if head_pos.x == 0 {
				cell::ARENA.width as i32 - 1
			} else {
				head_pos.x - 1
			};
			cell::Position { x, y: head_pos.y }
		}
		cell::Direction::Right => {
			let x = if head_pos.x == cell::ARENA.width as i32 - 1 {
				0
			} else {
				head_pos.x + 1
			};
			cell::Position { x, y: head_pos.y }
		}
		cell::Direction::Up => {
			let y = if head_pos.y == cell::ARENA.height as i32 - 1 {
				0
			} else {
				head_pos.y + 1
			};
			cell::Position { x: head_pos.x, y }
		}
		cell::Direction::Down => {
			let y = if head_pos.y == 0 {
				cell::ARENA.height as i32 - 1
			} else {
				head_pos.y - 1
			};
			cell::Position { x: head_pos.x, y }
		}
	};
	let mut last_segment_pos = mem::replace(&mut *head_pos, new_head_pos);

	if let Some(segment_color) = state.spawn_new_segment_color {
		state.body_segments.push_front(spawn_new_segment(
			last_segment_pos,
			segment_color,
			&mut cmds,
		));
		state.spawn_new_segment_color = None;
	} else {
		for seg in state.body_segments.iter().copied() {
			let mut pos = positions.get_mut(seg).unwrap();
			mem::swap(&mut *pos, &mut last_segment_pos);
		}
	}
}

pub fn spawn_new_segment(pos: cell::Position, color: Color, cmds: &mut Commands) -> Entity {
	cmds.spawn(SpriteBundle {
		sprite: Sprite {
			custom_size: Some(Vec2::new(10.0, 10.0)),
			color,
			..Default::default()
		},
		..Default::default()
	})
	.insert(BodySegment)
	.insert(cell::Size::square(0.6))
	.insert(pos)
	.id()
}

pub fn check_head_body_collision(
	mut app_state: ResMut<bevy::ecs::schedule::State<game::State>>,
	head_pos: Query<&cell::Position, With<Head>>,
	body_seg_poss: Query<&cell::Position, With<BodySegment>>,
) {
	if game::State::InGame != *app_state.current() {
		return;
	}

	let head_pos = *head_pos.single();
	let collision_detected = body_seg_poss
		.iter()
		.copied()
		.any(|seg_pos| seg_pos == head_pos);
	if collision_detected {
		app_state
			.set(game::State::GameOver)
			.expect("Unable to set game over state");
	}
}

pub fn snake_stunned_animation(time: Res<Time>, mut head_trans: Query<&mut Transform, With<Head>>) {
	let mut head_trans = head_trans.single_mut();
	let val = time.elapsed_seconds() % 2.0 / 2.0;
	head_trans.rotation = Quat::from_rotation_z(
		(::simple_easing::sine_in_out(::simple_easing::roundtrip(val)) - 0.5) * 0.75,
	);
}
