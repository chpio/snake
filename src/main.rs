use ::bevy::{
	diagnostic::{FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin},
	prelude::*,
	time::FixedTimestep,
};

mod cell;
mod food;
mod game;
mod snake;

fn main() {
	App::new()
		.insert_resource(Msaa { samples: 4 })
		.add_plugins(DefaultPlugins.set(WindowPlugin {
			window: WindowDescriptor {
				title: "snak".to_string(),
				width: 500.0,
				height: 500.0,
				present_mode: ::bevy::window::PresentMode::Fifo,
				..Default::default()
			},
			..default()
		}))
		.add_plugin(FrameTimeDiagnosticsPlugin::default())
		.add_plugin(LogDiagnosticsPlugin::default())
		.add_state(game::State::InGame)
		.insert_resource(snake::State::default())
		.add_startup_system(setup)
		.add_startup_system(game::setup_game.pipe(food::spawn_food))
		.add_system_set(
			SystemSet::on_enter(game::State::InGame).with_system(
				game::clear_game
					.pipe(game::setup_game)
					.pipe(food::spawn_food),
			),
		)
		.add_system_set(
			SystemSet::on_update(game::State::InGame).with_system(snake::process_snake_inputs),
		)
		.add_system(
			snake::move_snake
				.pipe(food::eat_food)
				.pipe(snake::check_head_body_collision)
				.with_run_criteria(FixedTimestep::step(0.25)),
		)
		.add_system(food::spawn_food.with_run_criteria(FixedTimestep::step(3.)))
		.add_system_set(
			SystemSet::on_update(game::State::GameOver)
				.with_system(game::check_for_input_game_restart)
				.with_system(snake::snake_stunned_animation),
		)
		// run after the default `CoreStage::Update` to also run over newly spawned entities
		.add_system_set_to_stage(
			CoreStage::PostUpdate,
			SystemSet::new()
				.with_system(crate::cell::position_translation)
				.with_system(cell::size_scaling),
		)
		.run();
}

fn setup(mut cmds: Commands) {
	cmds.spawn(Camera2dBundle::default());
}
