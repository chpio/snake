use crate::{cell, snake};
use bevy::{ecs, prelude::*};
use rand::{thread_rng, Rng};

#[derive(Debug, Clone, Eq, PartialEq, Hash, Resource)]
pub enum State {
	InGame,
	GameOver,
}

/// setups game objects in an empty (already cleared) world
pub fn setup_game(mut cmds: Commands, mut state: ResMut<snake::State>) {
	let head_pos = cell::Position {
		x: (cell::ARENA.width / 2.) as i32,
		y: (cell::ARENA.height / 2.) as i32 - 1,
	};

	let mut rng = thread_rng();
	state.body_segments.push_front(snake::spawn_new_segment(
		{
			let mut seg_pos = head_pos;
			seg_pos.x -= 1;
			seg_pos
		},
		Color::rgb(rng.gen(), rng.gen(), rng.gen()),
		&mut cmds,
	));

	cmds.spawn(SpriteBundle {
		sprite: Sprite {
			color: Color::rgb(1., 1., 1.),
			custom_size: Some(Vec2::new(50.0, 50.0)),
			..Default::default()
		},
		..Default::default()
	})
	.insert(snake::Head)
	.insert(cell::Size::square(0.9))
	.insert(head_pos);
}

pub fn check_for_input_game_restart(
	mut app_state: ResMut<ecs::schedule::State<State>>,
	keyboard_input: Res<Input<KeyCode>>,
) {
	if keyboard_input.pressed(KeyCode::Space) {
		app_state.set(State::InGame).unwrap();
	}
}

pub fn clear_game(
	mut cmds: Commands,
	entities: Query<Entity, With<cell::Position>>,
	mut state: ResMut<snake::State>,
) {
	// reset state
	*state = snake::State::default();

	// despawn all game entities
	for ent in entities.iter() {
		cmds.entity(ent).despawn();
	}
}
