use bevy::prelude::*;

#[derive(Component)]
pub struct Size {
	pub width: f32,
	pub height: f32,
}

impl Size {
	pub const fn square(x: f32) -> Self {
		Self {
			width: x,
			height: x,
		}
	}
}

#[derive(Default, Copy, Clone, Eq, PartialEq, Hash, Component)]
pub struct Position {
	pub x: i32,
	pub y: i32,
}

pub const ARENA: Size = Size::square(20.0);

#[derive(Copy, Clone, Eq, PartialEq)]
pub enum Direction {
	Left,
	Right,
	Up,
	Down,
}

impl Direction {
	pub fn reverse(self) -> Direction {
		match self {
			Direction::Left => Direction::Right,
			Direction::Right => Direction::Left,
			Direction::Up => Direction::Down,
			Direction::Down => Direction::Up,
		}
	}
}

pub fn position_translation(windows: Res<Windows>, mut q: Query<(&Position, &mut Transform)>) {
	fn convert(pos: f32, bound_window: f32, bound_game: f32) -> f32 {
		let tile_size = bound_window / bound_game;
		pos / bound_game * bound_window - (bound_window / 2.) + (tile_size / 2.)
	}

	if let Some(window) = windows.get_primary() {
		for (pos, mut transform) in q.iter_mut() {
			transform.translation = Vec3::new(
				convert(pos.x as f32, window.width(), ARENA.width),
				convert(pos.y as f32, window.height(), ARENA.height),
				0.0,
			);
		}
	}
}

pub fn size_scaling(windows: Res<Windows>, mut q: Query<(&Size, &mut Sprite)>) {
	if let Some(window) = windows.get_primary() {
		for (size, mut sprite) in q.iter_mut() {
			sprite.custom_size = Some(Vec2::new(
				size.width / ARENA.width * window.width(),
				size.height / ARENA.height * window.height(),
			));
		}
	}
}
