use crate::{cell, game, snake};
use bevy::prelude::*;
use rand::{thread_rng, Rng};
use std::iter;

#[derive(Component)]
pub struct Food;

pub fn spawn_food(
	app_state: Res<bevy::ecs::schedule::State<game::State>>,
	mut cmds: Commands,
	poss: Query<&cell::Position>,
) {
	if game::State::InGame != *app_state.current() {
		return;
	}

	let mut rng = thread_rng();
	let new_pos = iter::repeat_with(|| cell::Position {
		x: rng.gen_range(0..cell::ARENA.width as i32),
		y: rng.gen_range(0..cell::ARENA.height as i32),
	})
	// cap number of tries
	.take(10)
	.filter(|new_pos| {
		// check for no other objects on that cell
		poss.iter().all(|p| p != new_pos)
	})
	.next();

	if let Some(new_pos) = new_pos {
		cmds.spawn(SpriteBundle {
			sprite: Sprite {
				color: Color::rgb(rng.gen(), rng.gen(), rng.gen()),
				custom_size: Some(Vec2::new(10.0, 10.0)),
				..Default::default()
			},
			..Default::default()
		})
		.insert(Food)
		.insert(cell::Size::square(0.8))
		.insert(new_pos);
	}
}

pub fn eat_food(
	mut cmds: Commands,
	mut state: ResMut<snake::State>,
	head_pos: Query<&cell::Position, With<snake::Head>>,
	food_poss: Query<(Entity, &cell::Position, &Sprite), With<Food>>,
) {
	let head_pos = head_pos.single();
	let food = food_poss
		.iter()
		.find(|(_, food_pos, _)| head_pos == *food_pos);
	if let Some((food, _, sprite)) = food {
		state.spawn_new_segment_color = Some(sprite.color);
		cmds.entity(food).despawn();
	}
}
