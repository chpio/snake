fn main() {
	if cfg!(target_os = "linux") {
		// https://github.com/bevyengine/bevy/blob/main/docs/linux_dependencies.md#nixos
		println!("cargo:rustc-link-lib=vulkan");
	}
}
